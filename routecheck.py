from imposm.parser import OSMParser
import requests
import json
import time
import socket

class RoutingAPI(object):
    protocol = 'http'
    host = 'osm.fsffrance.org'
    port = '8989'
    response = {}

    def call(self, pfrom, pto):
        url = self.urlh() + self.upath.format(pfrom[1], pfrom[0],
                                              pto[1], pto[0])
        self.response = requests.get(url)        
        return self.response.content

class GraphHopperAPI(RoutingAPI):
    protocol = 'http'
    host = 'osm.fsffrance.org'
    port = '8989'
    response = {}

    upath = "/api/route?point={},{}&point={},{}&type=json&locale=fr"


    def urlh(self):
        """Return the host part of URL
        """
        return "{}://{}:{}" .format(self.protocol, self.host, self.port)

    def route_found(self):
        rfo = json.loads(self.response)
        return rfo['info']['routeFound']


# simple class that handles the parsed OSM data.
class HighwayCounter(object):
    highways = 0
    refs = []

    def nodes(self, ways):
        # callback method for ways
        for osmid, tags, ref in ways:
            self.refs.append(ref)


if __name__ == '__main__':
    # instantiate counter and parser and start parsing
    counter = HighwayCounter()
    p = OSMParser(concurrency=4, nodes_callback=counter.nodes)
    p.parse('ferry_terminal.osm')

    start = time.time()

    api = GraphHopperAPI()
    data = socket.gethostbyname_ex(api.host)
    api.host = data[2][0]

    print data[2][0]
    i = 0
    for ref_from in counter.refs:
        for ref_to in counter.refs:            
            if not ref_from == ref_to:
                i = i + 1
                elapsed = time.time() - start
                print "{}".format(i / elapsed)
                api.call(ref_from, ref_to)

