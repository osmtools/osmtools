#! /usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2012 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, sys
import syslog
import urllib2
sys.path.append('../')

import unittest

from pg2geojson import *
from StringIO import StringIO

class Pseudopt():
    def __init__(self, tags = None, wheres = None, nodes = True, dbname = None, fpath = None):
        self.tags = tags
        self.wheres = wheres
        self.limit = 42
        self.nodes = nodes
        self.prefix = "foo"
        self.file = fpath
        self.verbose = False
        self.dbname = dbname

class PseudoTgp():
    def __init__(self, fields = None, where = ''):
        self.fields = fields
        self.where = where 

class PseudoCursor():
    def __init__(self, fields = None, where = ''):
        self.fields = fields
        self.where = where 
    
    def execute(self, foo, bar):
        return "alpha"

    def fetchall(self):
        return [['alpha', 'foo'], ['bar','foobar']]

class Testspg2geojson(unittest.TestCase):

    def test_buildSql(self):
        opt = Pseudopt()
        tgp = PseudoTgp()
        result = "SELECT st_asgeojson(st_transform(way,4326)), st_geohash(st_transform(way,4326)) FROM foo_osm_point LIMIT 42"
        self.assertEqual (buildSql(opt, tgp), result, 'buildSql')

    def test_buildSqlWithFields(self):
        opt = Pseudopt()
        tgp = PseudoTgp('foo, foobar')
        result = "SELECT st_asgeojson(st_transform(way,4326)), st_geohash(st_transform(way,4326)), foo, foobar FROM foo_osm_point LIMIT 42"
        self.assertEqual (buildSql(opt, tgp), result, 'buildSql')

    def test_is_tag_int(self):
        self.assertFalse (is_tag_int("foo") , 'to_int with zero')
        self.assertTrue (is_tag_int("foo=i"), 'to_int with zero')
        self.assertFalse (is_tag_int("foo=f"), 'to_int with zero')

    def test_is_tag_float(self):
        self.assertFalse (is_tag_float("foo") , 'to_int with zero')
        self.assertFalse (is_tag_float("foo=i"), 'to_int with zero')
        self.assertTrue (is_tag_float("foo=f"), 'to_int with zero')


    def test_tagtag(self):
        self.assertEqual (tagtag("bar"), "bar", 'tagtag single')
        self.assertEqual (tagtag("foobar=i"), "foobar", 'tagtag with option')

    def test_wichOperator(self):
        tgprsr = tagParser( Pseudopt( ['foobar','alice'] ))
        self.assertEqual (tgprsr.wichOperator('foo>=bar'),  '>=', 'wichOperator')
        tgprsr = tagParser( Pseudopt( ['foobar','alice'] ))
        self.assertEqual (tgprsr.wichOperator('foo<=bar'),  '<=', 'wichOperator')
        tgprsr = tagParser( Pseudopt( ['foobar','alice'] ))
        self.assertEqual (tgprsr.wichOperator('foo>bar'),  '>', 'wichOperator')
        tgprsr = tagParser( Pseudopt( ['foobar','alice'] ))
        self.assertEqual (tgprsr.wichOperator('foo<bar'),  '<', 'wichOperator')
        tgprsr = tagParser( Pseudopt( ['foobar','alice'] ))
        self.assertEqual (tgprsr.wichOperator('foo=bar'),  '=', 'wichOperator')
        tgprsr = tagParser( Pseudopt( ['foobar','alice'] ))
        self.assertEqual (tgprsr.wichOperator('foobar'),  None, 'wichOperator')

    def test_tagParser(self):
        tgprsr = tagParser( Pseudopt( ['foobar','alice'] ))
        self.assertTrue (tgprsr.fields == "foobar,alice", 'tagParser')
        tgprsr = tagParser( Pseudopt( ['foobar','alice=i']))
        self.assertTrue (tgprsr.fields == "foobar,alice::int", 'tagParser with format')

        tgprsr = tagParser(Pseudopt(['foobar']))
        self.assertTrue (tgprsr.fields == "foobar", 'tagParser')

    def test_tagParserWhereOne(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],['place=town']))        
        self.assertEqual (tgprsr.where, "WHERE place=%s", 'tagParserWhere single')

    def test_tagParserWhereTwo(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],['place=town,city']))
        self.assertEqual (tgprsr.where, "WHERE ( place=%s OR place=%s )",
                          'tagParserWhere double : ' + tgprsr.where)

    def test_tagParserWhereThree(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],['place=town', 'foobar=bar']))
        self.assertEqual (tgprsr.where, "WHERE place=%s AND foobar=%s", 
                          'tagParserWhere three : ' + tgprsr.where)

    def test_tagParserWhereFour(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],['place=town,city', 'foobar>=bar']))
        self.assertEqual (tgprsr.where, "WHERE ( place=%s OR place=%s ) AND foobar>=%s", 
                          'tagParserWhere three : ' + tgprsr.where)

    def test_tagParserWhereFive(self):
        data = ['place=town,city,village', 'foobar=bar', 'is_in=France,Belgium']
        result = "WHERE ( place=%s OR place=%s OR place=%s ) AND foobar=%s AND ( is_in=%s OR is_in=%s )"
        tgprsr = tagParser(Pseudopt(['foobar=foo'], data))
        self.assertEqual (tgprsr.where, result, 
                          'tagParserWhere returned : ' + tgprsr.where)

    def test_tagSQLW(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],['place=town']))        
        self.assertTrue (tgprsr.sqlw(['foo','=','bar']) == "foo=%s"), 'tagSQLW'

    def test_wIdiomsSingle(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],['place=town']))
        self.assertEqual (tgprsr.wIdioms("foo"), " ( foo ) ", 'wIdioms returned : ' + tgprsr.wIdioms("foo")  ) 

    def test_wIdiomsDouble(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],None))
        data = ["foobar","foo"]
        attd = " ( foobar OR foo ) "
        result = tgprsr.wIdioms( data )
        self.assertTrue ( result == attd, 'wIdioms returned : ' + result ) 


    def test_tgpcleanspaces(self):
        tgprsr = tagParser(Pseudopt(['foobar=foo'],None))
        data = " Lorem ipsum   dolor sit amet, consectetur  adipiscing  elit. "
        attd = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        result = tgprsr.cleanSpaces( data )
        self.assertTrue ( result == attd, 'cleanSpaces returned : ' + result ) 

    def test_cleanspaces(self):
        data = " Lorem ipsum   dolor sit amet, consectetur  adipiscing  elit. "
        attd = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
        result = cleanSpaces( data )
        self.assertTrue ( result == attd, 'cleanSpaces returned : ' + result ) 


    def test_to_int(self):
        self.assertEqual (to_int(0), 0,     'to_int with zero')
        self.assertEqual (to_int(None), 0, 'to_int with None')
        self.assertEqual (to_int(1), 1,     'to_int with one')
        self.assertEqual (to_int("42"), 42, 'to_int with one')

    def test_to_float(self):
        self.assertEqual (to_float(0), 0.0,     'to_float with zero')
        self.assertEqual (to_float(None), 0.0, 'to_float with None')
        self.assertEqual (to_float(1), 1.0,     'to_float with one')
        self.assertEqual (to_float("42"), 42.0, 'to_float with one')


    def test_output_data(self):
        opt = Pseudopt()
        self.assertEqual (output_data(opt, "foobar"), 0, 'ouput_data')

        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            output_data(opt, "foobar")
            output = out.getvalue().strip()
            self.assertEqual(output, '"foobar"', "output_data")
        finally:
            sys.stdout = saved_stdout

    def test_output_dataToFile(self):
        opt = Pseudopt(fpath = '/tmp/foobar')
        self.assertEqual (output_data(opt, "foobar"), 0, 'ouput_data')

        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            output_data(opt, "foobar")
            output = out.getvalue().strip()
            self.assertEqual(output, '', "output_data")
            self.assertTrue(os.path.exists('/tmp/foobar'), "output_data")
        finally:
            sys.stdout = saved_stdout


    def test_array2json(self):
        records = [['''{"type":"Point","coordinates":[0.779775999891454,9.261274698722048]}''','s18s9tzyf4epkpe4hvv3','lorem', 2]]
        tags = ['foo','bar=i']
        attd = {"type": "FeatureCollection", 
                "features": [{"geometry": {"type": "Point", "coordinates": [0.80044229988857696, 10.847461498508066]}, 
                              "type": "Feature", 
                              "properties": {"place": "town", "population": 11000}}]
                }
        result = array2json(records, tags)
        result_str = " ".join(['%s:: %s' % (key, value) for (key, value) in result.items()])
        self.assertEqual(type(result).__name__, "dict", 'array2json' + result_str)
        self.assertEqual(result['type'], "FeatureCollection", 'array2json' + result_str)
        self.assertEqual(len(result['features']), 1, 'array2json' + result_str)

    def test_array2json2(self):
        records = [ ['''{"type":"Point","coordinates":[0.779775999891454,9.261274698722048]}''','s18s9tzyf4epkpe4hvv3','lorem', 2],
                    ['''{"type":"Point","coordinates":[0.779775999891454,9.261274698722048]}''','s18s9tzyf4epkpe4hvv3','lorem', 2],
                    ['''{"type":"Point","coordinates":[0.779775999891454,9.261274698722048]}''','s18s9tzyf4epkpe4hvv3','lorem', 2]]
        tags = ['foo','bar=i']
        attd = {"type": "FeatureCollection", 
                "features": [{"geometry": {"type": "Point", "coordinates": [0.80044229988857696, 10.847461498508066]}, 
                              "type": "Feature", 
                              "properties": {"place": "town", "population": 11000}}]
                }
        result = array2json(records, tags)
        result_str = " ".join(['%s:: %s' % (key, value) for (key, value) in result.items()])
        self.assertEqual(type(result).__name__, "dict", 'array2json' + result_str)
        self.assertEqual(result['type'], "FeatureCollection", 'array2json' + result_str)
        self.assertEqual(len(result['features']), 3, 'array2json' + result_str)
        self.assertEqual(len(result['features'][0]['properties']), 2, 'array2json' + result_str)


    def test_print_usage(self):
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            print_usage()
            output = out.getvalue().strip()
            self.assertTrue(len(output) > 10, "print_usage")
        finally:
            sys.stdout = saved_stdout

    def test_check_optionsWoNodes(self):
        """
        Nodes not selected on command line
        """
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            check_options( Pseudopt(None, None, False, "foo") )
            output = out.getvalue().strip()
            self.assertTrue(len(output) > 10, "check_options")
        finally:
            sys.stdout = saved_stdout

    def test_check_optionsWiNodes(self):
        """
        Nodes selected on command line
        """
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            check_options( Pseudopt(None, None, True, "foo") )
            output = out.getvalue().strip()
            self.assertTrue(len(output) == 0, "check_options" + output)
        finally:
            sys.stdout = saved_stdout

    def test_check_optionsWoDbname(self):
        """
        dbname omitted selected on command line
        """
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            check_options( Pseudopt(None, None, True, None) )
            output = out.getvalue().strip()
            self.assertTrue(len(output) > 10, "check_options")
        finally:
            sys.stdout = saved_stdout

    def test_check_optionsWiDbname(self):
        """
        dbname indicated on commend line        
        """
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            check_options( Pseudopt(None, None, True, "foo") )
            output = out.getvalue().strip()
            self.assertTrue(len(output) == 0, "check_options" + output)
        finally:
            sys.stdout = saved_stdout


    def test_fetch_datas(self):
        """
        fetch_datas from db
        """
        opt= Pseudopt(['foo'], None, True, None)
        cur = PseudoCursor()
        saved_stdout = sys.stdout
        try:
            out = StringIO()
            sys.stdout = out
            result = fetch_datas( cur, opt )
            output = out.getvalue().strip()
            self.assertTrue(len(output) > 10, "fetch_datas")
            self.assertEqual(len(result), 2, "fetch_datas")
        finally:
            sys.stdout = saved_stdout

suite = unittest.TestLoader().loadTestsFromTestCase(Testspg2geojson)
unittest.TextTestRunner(verbosity=2).run(suite)
