#!/usr/bin/perl
#
# Copyright (C) 2008,2011 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Debian packages : 
#  - libgetopt-mixed-perl
#  - libcache-memcached-fast-perl

use strict;
use Getopt::Long;
use Cache::Memcached::Fast;
use vars qw(@servers @layers %badstatus);

=head1 DESCRIPTION

    clean-tilecache.pl removes tiles from memcached server. Deletion
    are made for one or more specified layer, and between 2 zoom
    levels.

=head1 SYNOPSIS

    clean-tilememcache.pl --layer=foobar [--layer=barfoo] 
    [--zmin=5] [--zmax=12] [--verbose] [--server=127.0.0.1] [--port=11211]

=cut

# default server address/port
my $server = "127.0.0.1";
my $port = "11211";

my ($verbose, $debug) = (0, 0);
my ($zmin, $zmax) = (2, 4);



my $result = GetOptions ( "layer=s" => \@layers,
			  "verbose" => \$verbose,
			  "debug" => \$debug,
			  "server=s" => \$server,
			  "port=s" => \$port,
			  "zmin=i" => \$zmin,
			  "zmax=i" => \$zmax);


my $memd = new Cache::Memcached::Fast({
    servers => [ { address => "$server:$port" }, ],
    namespace => '',
    connect_timeout => 0.2,
    io_timeout => 0.5,
    close_on_error => 1,
    compress_threshold => 100_000,
    compress_ratio => 0.9,
    max_failures => 3,
    failure_timeout => 2,
    ketama_points => 150,
    nowait => 1,
    hash_namespace => 1, });



if ( scalar(@layers) == 0) {
    print "Need at least one layer\n";
    exit 1;
}

# Remove duplicates layer
my (%hlayer);
%hlayer->{$_} = $_ foreach (@layers);
(@layers) = (keys(%hlayer));
#

# check correct values on zoom
$zmax = 18 if $zmax > 18;
$zmin = 0 if $zmin < 0;
my $nbtiles = 0;
my ($tiles, $inc) = (0,0);

my $pid = fork();
if (not defined $pid) {
    clean_loop(0, 0);
} elsif ($pid == 0) {
    # children
    clean_loop(0, 1);
    exit(0);
} else {
    # father
    clean_loop(1, 1 );
    waitpid($pid,0);
}

sub clean_loop() {
    my ($start, $inc) = @_;
    
    foreach my $layer (@layers) {
	for ( my $z = $zmin ; $z < $zmax+1 ; $z++ ) {
	    
	    $nbtiles = (2 ** $z ) ;
	    
	    for ( my $j = 0 ; $j < $nbtiles ; $j++ ) {
		for ( my $i = $start ; $i < $nbtiles ; $i = $i + 1 + $inc ) {
		    my $key = sprintf "%s/%s/%s/%s", $layer, $j, $i, $z;
		    print "$key\n" if $verbose;
		    $memd->delete($key, 'text');
		    $tiles++;
		}
	    }
	}
    }    
}

printf "%s tiles deleted from memcached server\n", $tiles;


=head1 AUTHOR

    Rodolphe Quiedeville <rodolphe@quiedeville.org>

=head1 LICENCE

    GPLv3 licence

=head1 SEE ALSO

    Last versions of clean-tilecache are available at https://gitorious.org/osmtools

=cut
