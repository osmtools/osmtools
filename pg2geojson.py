#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2012 Rodolphe Quiédeville <rodolphe@quiedeville.org> 

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 pg2geojson extract data from an osm database to geojson

 Reads data from a postgres database previously loaded with osm2pgsql,
 generate a geojson file. Can filter on tag with logical operator =, <, >

 Usage :

   pg2geojson --node --tag name --tag population=i --where place=city,town

"""

import psycopg2
import psycopg2.extras
import sys
import simplejson as json
from optparse import OptionParser

__version__ = "0.2"

class tagParser():
    """

    """
    def __init__(self, options):
        self.where = ''
        self.fields = None
        self.options = options.tags
        self.wheres = options.wheres
        self.parms = ()

        self.parseFields()
        if self.wheres != None:
            self.parseWhere()

    def parseFields(self):
        """
        """
        fields = []
        field_string = None

        for tag in self.options:
            part = tag.split('=')
            if len(part) > 1:
                if part[1] == 'i':
                    fields.append( "%s::int" % (part[0]) )
                else:
                    fields.append( part[0] )
            else:
                fields.append( part[0] )                

        if len(fields) > 0:
            field_string = fields.pop(0)
            for fieldx in fields:
                field_string = field_string + u',' + fieldx
            
        self.fields = field_string

    def parseWhere(self):
        """
        Build the WHERE clause from command line options
        """
        where_string = ''
        clauses = []
        where_parms = []

        for tag in self.wheres:
            operator = self.wichOperator(tag)
            part = tag.split(operator)
            values = part[1].split(',')
            idioms = []
            if len(values) > 1:
                for val in values:
                    idioms.append(self.sqlw( [part[0], operator ]) )
                    where_parms.append(val)
                clauses.append( self.wIdioms(idioms) )
            else:
                clauses.append( self.sqlw( [part[0], operator ]))
                where_parms.append(part[1])

        if len(clauses) > 0:
            where_string = u" WHERE " + clauses.pop(0)
            for fieldx in clauses:
                where_string = where_string + u' AND ' + fieldx
            
        self.where = self.cleanSpaces(where_string)
        if len(where_parms) > 0:
            self.parms = tuple(where_parms)

    def wichOperator(self, tag):
        """
        Return the operator string
        """
        if '<=' in tag:
            return '<='
        elif '>=' in tag:
            return '>='
        elif '>' in tag:
            return '>'
        elif '<' in tag:
            return '<'
        elif '=' in tag:
            return '='
        else:
            return None

    def sqlw(self, datas):
        """
        Return 
        """
        values = u"%s%s%%s" % (datas[0], datas[1])
        return values

    def wIdioms(self, values):
        """
        Build each part of where clause
        """
        if type(values).__name__ == 'list':
            qry = u" ( " + values.pop(0)
            for val in values:
                qry = qry + u''' OR ''' + val
            qry = qry + " ) "
        else:
            qry = " ( %s ) " % (values)
                
        return qry

    def cleanSpaces(self, values):
        """
        Remove all double spaces in a string
        """
        cval = " ".join(values.split())
        return cval


def cleanSpaces(values):
    """
    Remove all double spaces in a string
    """
    cval = " ".join(values.split())
    return cval

def buildSql(options, tgp):
    """
    Build SQL query
    """

    if options.nodes:
        table = options.prefix + u'_osm_point'

    qry = "SELECT st_asgeojson(st_transform(way,4326)), st_geohash(st_transform(way,4326))"

    if tgp.fields != None:
        qry = qry + ", " + tgp.fields
    
    qry = cleanSpaces(qry + u' FROM ' + table + u' ' + tgp.where + u''' LIMIT ''' + str(options.limit))
    print qry
    return qry

def fetch_datas(cursor, options):

    tgp = tagParser(options)

    query = buildSql(options, tgp)
    args = tgp.parms

    try:
        cursor.execute(query, args)
    except psycopg2.ProgrammingError, error:
        print error
        sys.exit(2)

    # retrieve the records from the database
    records = cursor.fetchall()
    print "found %d tuples" % (len(records))
    return records

def arg_parse():
    """ Parse command line arguments """
    arg_list = "-d -g [-fhoprstv] [-p NUM] [-c FILENAME]"
    usage = "Usage: %prog " + arg_list + """

 extract the tag 'population' from all cities where the tag place equal to 'city' or 'town'

 %prog -n -d foobar -t population -w place=city,town

 extract the tags 'population' and 'is_in' from all cities where the tag place equal 
 to 'city' and admin_level=8 ; convert population values to integer

 %prog -n -d foobar -t population=i -t is_in -w place=city -w admin_level=8"""
    parser = OptionParser(usage, version=__version__)
    parser.add_option("-t", "--tags", dest="tags",
                      action="append", 
                      help="read tag list", 
                      default=None)
    parser.add_option("-d", "--dbname", dest="dbname",
                      help="read tag list", 
                      default=None)
    parser.add_option("-f", "--file", dest="file",
                      help="output to file", 
                      default=None)
    parser.add_option("-l", "--limit", dest="limit",
                      help="output to file", 
                      default=100)
    parser.add_option("-n", "--nodes", dest="nodes",
                      action="store_true", 
                      help="extract nodes",
                      default=False)
    parser.add_option("-p", "--prefix", dest="prefix",
                      help="database tables prefix",
                      default="planet")
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true",
                      help="verbose mode",
                      default=False)
    parser.add_option("-w", "--where", dest="wheres",
                      action="append", 
                      help="where clause list", 
                      default=None)
    return parser.parse_args()[0]

def check_options(options):
    """
    Check mandatory options
    """
    if options.nodes == False:
        print_usage()
        return 1

    if options.dbname == None:
        print_usage()
        return 2
    return 0

def print_usage():
    """
    Print usage on stdin and exits
    """
    print "Usage -d DBNAME --[nodes|ways]"

def is_tag_int(tag):
    """
    Detect if the tag is declared as integer
    """
    result = False
    part = tag.split('=')
    if len(part) > 1:
        if part[1] == 'i':
            result = True
    return result

def is_tag_float(tag):
    """
    Detect if the tag is declared as float
    """
    result = False
    part = tag.split('=')
    if len(part) > 1:
        if part[1] == 'f':
            result = True
    return result


def tagtag(tag):
    part = tag.split('=')
    return part[0]

def array2json(records, tagg):
    """
    Return a geojson object

    records (list)

    tagg (list)
    """
    features = []

    for record in records:
        geom = json.loads(record[0])
        prop = {}
        i = 2
        for tag in tagg:
            if is_tag_int(tag):
                prop[tagtag(tag)] = to_int(record[i])
            elif is_tag_float(tag):
                prop[tagtag(tag)] = to_float(record[i])
            else:
                prop[tagtag(tag)] = record[i]
            i = i + 1
        feature = { "type": "Feature", 
                    "geometry": geom,
                    "properties": prop }
        features.append(feature)

    data = { "type": "FeatureCollection", "features": features }
    return data

def to_int(value):
    """
    Convert string or null value to int
    """
    if value == None:
        return 0
    else:
        return int(value)

def to_float(value):
    """
    Convert string or null value to float
    """
    if value == None:
        return 0
    else:
        return float(value)


def output_data(options, datas):
    """
    Output datas to STDIN or file
    """
    if options.file == None:
        print json.dumps(datas)
    else:
        if options.verbose:
            print "Output result to file : %s" % (options.file)
        try:
            fpo = open(options.file, 'w')
            json.dump(datas, fpo)
            fpo.close()
        except IOError, error:
            print error[1]
            sys.exit(2)
    return 0

def main():
    """
    main function
    """
    #start the script
    options = arg_parse()
    if check_options(options) != 0:
        sys.exit(1)

    conn_string = "host='localhost' dbname='"+options.dbname+"' "
    try:
        # get a connection, if a connect cannot be made an exception will be raised here
        conn = psycopg2.connect(conn_string)
        # conn.cursor will return a cursor object, you can use this query to perform queries
        # note that in this example we pass a cursor_factory argument that will
        # dictionary cursor so COLUMNS will be returned as a dictionary so we
        # can access columns by their name instead of index.
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    except:
        # Get the most recent exception
        exceptionType, exceptionValue, exceptionTraceback = sys.exc_info()
        # Exit the script and print an error telling what happened.
        sys.exit("Database connection failed!\n ->%s" % (exceptionValue))

    records = fetch_datas(cursor, options)

    data = array2json(records, options.tags)

    output_data(options, data)


if __name__ == "__main__":
    sys.exit(main())
