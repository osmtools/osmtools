#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from imposm.parser import OSMParser
import requests
import json
import time
import sys
import psycopg2
import socket

class RoutingAPI(object):
    protocol = 'http'
    host = 'osm.fsffrance.org'
    port = '8989'
    response = {}
    djson = None

    def call(self, pfrom, pto):
        url = self.urlh() + self.upath.format(pfrom[1], pfrom[0],
                                              pto[1], pto[0])
        self.response = requests.get(url)
        self.djson = None
        return self.response.content

    def json(self):
        if self.djson is None:
            try:
                self.djson = json.loads(self.response.content)
            except:
                self.djson = json.loads('{"status": "error"}')

        return self.djson

class GraphHopperAPI(RoutingAPI):
    protocol = 'http'
    host = 'graphhopper.com'
    port = '80'
    response = {}

    upath = "/routing/api/route?point={},{}&point={},{}&type=json&locale=fr"


    def urlh(self):
        """Return the host part of URL
        """
        return "{}://{}:{}" .format(self.protocol, self.host, self.port)

    def route_found(self):
        rfo = self.json()
        return rfo['info']['routeFound']

    def distance(self):
        rfo = self.json()
        return rfo['route']['distance']

class OsrmAPI(RoutingAPI):
    protocol = 'http'
    host = 'router.project-osrm.org'
    port = '80'
    response = {}

    upath = "/viaroute?loc={},{}&loc={},{}&output=json"


    def urlh(self):
        """Return the host part of URL
        """
        return "{}://{}:{}" .format(self.protocol, self.host, self.port)

    def route_found(self):
        rfo = self.json()
        if rfo['status'] != 'error':
            return rfo['status']
        else:
            return False

    def distance(self):
        rfo = self.json()
        if rfo['status'] != 'error':
            return rfo['route_summary']['total_distance']
        else:
            return False
