#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from imposm.parser import OSMParser
import requests
import json
import time
import sys
import psycopg2
import socket
from OsmTools import GraphHopperAPI
from OsmTools import OsrmAPI

if __name__ == '__main__':
    points = []
    points.append((4.734874, 45.638198))
    points.append((4.795279,45.649084))
    points.append((4.838455,45.638107))
    points.append((4.864825,45.749101))
    points.append((4.925099,45.751648))

    api = GraphHopperAPI()
    data = socket.gethostbyname_ex(api.host)
    api.host = data[2][0]

    result = api.call(points[0], points[1])
    distance = api.distance()
    print "{} {}".format(api.route_found(), distance)


    api = OsrmAPI()
    data = socket.gethostbyname_ex(api.host)
    api.host = data[2][0]

    result = api.call(points[0], points[1])
    distance = api.distance()
    print "{} {}".format(api.route_found(), distance)

