#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ovpwild execute as many query as necessary to cover the bbox get on
 command line options by square size of 1°x1° and store each request
 in a different file.

"""

import urllib
import urllib2
import os
import sys
from time import sleep
from optparse import OptionParser

__version__ = "0.2"
__server__ = 'http://api.openstreetmap.fr/oapi/interpreter'


def parse_file(fpath):
    """
    Open a file, return body
    """
    content = open(fpath, 'r').read()
    return content


def store_result(fpath, data):
    """
    Open a file, return body
    """
    open(fpath, 'w').write(data)


def arg_parse():
    """ Parse command line arguments """
    arg_list = "-q FILENAME -n NORTH -w WEST -s SOUTH -e EAST [-o OUTPUT-DIR] [-a API-HOST]"
    usage = "Usage: %prog " + arg_list + """
"""
    parser = OptionParser(usage, version=__version__)
    parser.add_option("-q", "--query", dest="query",
                      help="The query filepath",
                      default=None)
    parser.add_option("-n", "--north", dest="north",
                      help="the max latitude",
                      type="float",
                      default=None)
    parser.add_option("-s", "--south", dest="south",
                      help="the min latitude",
                      type="float",
                      default=None)
    parser.add_option("-w", "--west", dest="west",
                      help="the min longitude",
                      type="float",
                      default=None)
    parser.add_option("-e", "--east", dest="east",
                      help="the max longitude",
                      type="float",
                      default=None)
    parser.add_option("-o", "--output-dir", dest="output_dir",
                      help="the output directory where to store the result",
                      default="/tmp")
    parser.add_option("-a", "--api-url", dest="host",
                      help="the api server url",
                      default=__server__)
    parser.add_option("-u", "--resume", dest="resume",
                      help="start on query number #RESUME",
                      type="int",
                      default=0)
    parser.add_option("-g", "--user-agent", dest="useragent",
                      help="""user-agent to use in HTTP headers
(default ovpwild.py), it is always a good idea to add
an email to contact you when you use APIs""",
                      type="string",
                      default="ovpwild.py")
    parser.add_option("-r", "--retry", dest="retry",
                      help="number of retries on HTTP error 504, default 1",
                      type="int",
                      default=0)
    parser.add_option("-t", "--timeout", dest="timeout",
                      help="timeout when calling api server",
                      type="int",
                      default=120)
    parser.add_option("--size", dest="sqsize",
                      help="square size in degrees",
                      type="int",
                      default=1)
    parser.add_option("--sleep", dest="sleep",
                      help="time waited between 2 api request in second",
                      type="int",
                      default=2)

    return parser.parse_args()[0]


def check_options(options):
    """
    Check mandatory options
    """
    if options.north is None:
        print_usage()
        return 1

    if options.east is None:
        print_usage()
        return 1

    if options.south is None:
        print_usage()
        return 1

    if options.west is None:
        print_usage()
        return 1

    if options.query is None:
        print_usage()
        return 1

    return 0


def print_usage():
    """
    Print usage on stdin and exits
    """
    print "Usage -q QUERYFILE -n -w -s -e"


def urlparse(options, query):
    """
    open an url and return information
    """
    values = {'data': query}

    data = urllib.urlencode(values)
    req = urllib2.Request(options.host, data)
    req.add_header('User-Agent', options.useragent)

    inc = 0
    con = 0
    error = 0

    while inc <= options.retry and con == 0:
        try:
            if inc > 0:
                print "Retry %s on error %s" % (inc, error)
            handle = urllib2.urlopen(req, timeout=options.timeout)
            content = handle.read()
            con = 1
        except urllib2.HTTPError, err:
            error = err.code
            if error != 504:
                con = 1
            sys.exit("Exit on HTTP error %s" % error)
        inc = inc + 1

    return content


def main():
    """
    main function
    """
    #start the script
    options = arg_parse()
    if check_options(options) != 0:
        sys.exit(1)

    if options.north < options.south:
        flip = options.north
        options.north = options.south
        options.south = flip

    if options.east < options.west:
        flip = options.east
        options.east = options.west
        options.west = flip

    content = parse_file(options.query)

    if options.sqsize < 1:
        options.sqsize = 1

    if options.retry < 1:
        options.retry = 1

    if options.retry > 10:
        options.retry = 10

    lat = options.south
    print "Query overpass-api host : %s" % (options.host)
    print "The bbox is %s,%s,%s,%s" % (options.west,
                                       options.south,
                                       options.east,
                                       options.north)

    i = 1

    while lat < options.north:
        lon = options.west
        while lon < options.east:

            fpath = os.path.join(options.output_dir, "overpass%s.osm" % i)

            top = min(lat+options.sqsize, options.north)
            right = min(lon+options.sqsize, options.east)

            query = content.format(north=top,
                                   south=lat,
                                   west=lon,
                                   east=right)

            msg = "Query %s bbox lat:[%s %s] lon:[%s %s] will be store in %s"

            if options.resume == 0 or (options.resume > 0 and
                                       i >= options.resume):
                result = urlparse(options, query)
                store_result(fpath, result)
                sleep(options.sleep)
            else:
                msg = msg + " passed (resume mode)"

            print msg % (i,
                         lat,
                         top,
                         lon,
                         right,
                         fpath)

            lon = lon + options.sqsize
            i = i + 1
        lat = lat + options.sqsize


if __name__ == "__main__":
    sys.exit(main())
