#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ovpwild execute as many query as necessary to cover the bbox get on
 command line options by square size of 1°x1° and store each request
 in a different file.

"""
import sys
import json
import requests
import urllib

urlsearch = 'http://nominatim.quiedeville.org/search.php?{}'

bar = [{"place_id":"18863805",
        "licence":"Data \u00a9 OpenStreetMap contributors, ODbL 1.0. http:\/\/www.openstreetmap.org\/copyright",
        "osm_type":"node",
        "osm_id":"2608506537",
        "boundingbox":["49.3481254577637","49.3481292724609","6.18385314941406","6.18385362625122"],
        "lat":"49.3481274",
        "lon":"6.1838533",
        "display_name":"12, Rue du Poitou, Basse Yutz, Yutz, Thionville-Est, Moselle, Lorraine, France m\u00e9tropolitaine, 57100, France",
        "class":"place",
        "type":"house",
        "importance":0.701},
       {"place_id":"40585653","licence":"Data \u00a9 OpenStreetMap contributors, ODbL 1.0. http:\/\/www.openstreetmap.org\/copyright","osm_type":"way","osm_id":"110129612","boundingbox":["50.346378326416","50.3465270996094","3.53110790252686","3.53127002716064"],"lat":"50.3464535","lon":"3.53118056014151","display_name":"12, Rue du Poitou, Clos Cabernet, Faubourg de Cambrai, Valenciennes, Nord, Nord-Pas-de-Calais, France m\u00e9tropolitaine, 59770, France","class":"place","type":"house","importance":0.701},{"place_id":"35496595","licence":"Data \u00a9 OpenStreetMap contributors, ODbL 1.0. http:\/\/www.openstreetmap.org\/copyright","osm_type":"way","osm_id":"83234242","boundingbox":["48.811840057373","48.8120460510254","2.30748248100281","2.30778765678406"],"lat":"48.81194335","lon":"2.30759059049249","display_name":"12, Rue du Poitou, Montrouge, Antony, Hauts-de-Seine, \u00cele-de-France, France m\u00e9tropolitaine, 92120, France","class":"place","type":"house","importance":0.701}]

foo = [{"place_id":"34482985",
        "licence":"Data \u00a9 OpenStreetMap contributors, ODbL 1.0. http:\/\/www.openstreetmap.org\/copyright",
        "osm_type":"way",
        "osm_id":"77155701",
        "boundingbox":["50.2862739562988","50.2872085571289","2.80028057098389","2.80136513710022"],
        "lat":"50.2867236","lon":"2.8006479",
        "display_name":"Rue de la Fontainerie, Arras, Pas-de-Calais, Nord-Pas-de-Calais, France m\u00e9tropolitaine, 62051, France",
        "class":"highway","type":"residential","importance":1.3}]


if len(sys.argv) == 1:
    sys.stdout.write("usage {} FILENAME\n".format(sys.argv[0]))
    sys.exit(1)

fpath = sys.argv[1]

f = open(fpath, 'r')

lines = f.read()

i=0

errors = []

print "#lat,lon,name"

for line in (lines.split('\n')):
    
    url = urlsearch.format(urllib.urlencode({'q': "{}".format(line), 'format': 'json'}))

    response = requests.get(url.format(line))
    data = json.loads(response.text)

    if len(data):
        if data[0]["osm_type"] == "way":
            print "{},{},{}".format(data[0]["lat"], data[0]["lon"],line)
        else:
            errors.append(line)
    else:
        errors.append(line)

print errors
