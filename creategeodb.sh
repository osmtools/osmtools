#!/bin/sh

# Copyright (C) 2011,2012 Rodolphe Quiédeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Version : 1.1
#
dbname=$1
owner=$2

CREATEDB="/usr/bin/createdb"
CREATELANG="/usr/bin/createlang"
PSQL="/usr/bin/psql"

VERSION=`$PSQL -d template1 -c 'select version()' | grep 'PostgreSQL' | sed 's/.*PostgreSQL \(.*\) on .*/\1/' | cut -d '.' -f 1-2`

if [ -z $dbname ]; then
    echo "Usage : creategeodb.sh DBNAME [OWNER]"
    exit 1
fi

if [ -z $owner ]; then
    owner=`/usr/bin/whoami`
    echo "will use $owner as database's owner"
fi

$CREATEDB -O $owner $dbname || exit 1

if [ $VERSION = "8.4" ]; then
    $CREATELANG plpgsql $dbname || exit 1
    FILEP="/usr/share/postgresql/8.4/contrib/postgis-1.5/postgis.sql"
    FILES="/usr/share/postgresql/8.4/contrib/postgis-1.5/spatial_ref_sys.sql"
elif [ $VERSION = "9.1" ]; then
    # plpgsql exists by default in pg9.1
    FILEP="/usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql"
    FILES="/usr/share/postgresql/9.1/contrib/postgis-1.5/spatial_ref_sys.sql"
else
    echo "Version "$VERSION" is not supported yet, send an email to rodolphe@quiedeville.org to fix it."
    exit 1
fi


if [ -f $FILEP ]; then
    $PSQL -d $dbname -f $FILEP
fi

if [ -f $FILES ]; then
    $PSQL -d $dbname -f $FILES
fi

for table in "spatial_ref_sys" "geometry_columns"; do
    $PSQL -d $dbname -c 'ALTER TABLE  '$table' OWNER TO '$owner 
done

$PSQL -d $dbname -c 'ALTER VIEW geography_columns OWNER TO '$owner

if [ $VERSION = "9.1" ]; then
    $PSQL -d $dbname -c 'CREATE EXTENSION hstore'
fi
