#!/bin/bash

# Author : Rodolphe Quiédeville <rodolphe@quiedeville.org>
# Licence GPL
#
#
# Store the files to $DDIR, chnage it to suit your needs
DDIR="/home/rodo/doc/gpx"

FILENAME=`date +%y%m%d-%H%M`.gpx
MONTHYEAR=`date +%Y/%m`
# we use sudo to remode kernel module
# remeber to configure your sudoer file
RMMOD="/usr/bin/sudo /sbin/rmmod"
#
GREP=/bin/grep
MODULE_GARMIN=garmin_gps
MKDIR=/bin/mkdir
MDIR=$DDIR"/"$MONTHYEAR

if [ ! -d $DDIR ]; then
    print "$DDIR does not exists"
    exit 1
fi


$MKDIR -p "$MDIR/track"
$MKDIR -p "$MDIR/waypoint"

TRACKNAME=$MDIR"/track/track-"$FILENAME
POINTNAME=$MDIR"/waypoint/waypoint-"$FILENAME

if [ `/bin/lsmod | $GREP -c $MODULE_GARMIN` -gt 0 ]; then
    echo "$RMMOD $MODULE_GARMIN"
    $RMMOD $MODULE_GARMIN
fi

if [ ! -f $TRACKNAME ]; then
    echo 'Fetch tracks in '$TRACKNAME
    sudo gpsbabel -t -i garmin -f usb:. -o gpx -F $TRACKNAME
else
    echo $TRACKNAME" still exists"
fi

if [ ! -f $POINTNAME ]; then
    echo 'Fetch waypoints in '$POINTNAME
    sudo gpsbabel -w -i garmin -f usb:. -o gpx -F $POINTNAME
else
    echo $POINTNAME" still exists"
fi
#
# Now we try to rename tracks by track datetime
# we fetch the first point datetime
DTIME=`$GREP time $TRACKNAME | head -2 | tail -1 | sed -e 's/ *<[^>]*>//g'`

if [ ! -z $DTIME ]; then

    DATE=`echo $DTIME | cut -d 'T' -f 1`
    MONTH=`echo $DTIME | cut -d '-' -f 2`
    YEAR=`echo $DTIME | cut -d '-' -f 1`

    $MKDIR -p $DDIR"/"$YEAR"/"$MONTH"/track/"

    NEWNAME=$DDIR"/"$YEAR"/"$MONTH"/track/track-"$DTIME".gpx"
    if [ ! -f $NEWNAME ]; then
	echo "Move $TRACKNAME to $NEWNAME"
	mv $TRACKNAME $NEWNAME
    else
	echo "Not moving to $NEWNAME file exists"
    fi

fi