#!/usr/bin/perl
#
# Copyright (C) 2008,2011,2013 Rodolphe Quiedeville <rodolphe@quiedeville.org> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Debian packages : 
#  - libgetopt-mixed-perl
#  - libwww-perl

use strict;
use Getopt::Long;
use LWP::UserAgent;
use Math::Trig;
use vars qw(@servers @layers %badstatus %status_counter);

=head1 DESCRIPTION

    fill-cache.pl call on all servers the tiles inside a bbox

=head1 SYNOPSIS

    fill-cache.pl --layer=foobar [--layer=barfoo] 
    --server=one.domain [--server=two.domain:port]
    --bbox=latmin,lonmin,latmax,lonmax
    [--zmin=5] [--zmax=12] 
    [--verbose] 

=cut 

use constant VERSION => "1.1.0";

=head1 OPTIONS
    
    --bbox, bounding box in lonmin,latmin,lonmax,latmax traditionnal syntax.

    --point, lon, lat coordinate, fill cache center on this point

    --layer, layer name to be fetched. You can specify as many as you
      want --layer option when you want to fille the cache with more
      than one layer.

    --minwait, minimal time in second to wait beetween loops, (default 2 seconds)

    --purge when this option is est the url is called with PURGE
      instead of GET, permits to purge datas from varnish cache.

    --quiet, be silent, remove all output

    --server, name of server to be called, you can indicate more than
      one --server option. Server address can be followed by tcp port
      like server.domain:port. When port is not specified the port 80
      is used.    

    --simulate, simulate what will be done, no HTTP call is made when
      this option is set. Prints some stats data and exit.

    --timeout, HTTP timeout in second used (default 30 sec)

    --verbose, be verbose print on STDOUT what is done

    --version, show programm version number and exit.

    --zmax, maximum zoom lever (default 6)

    --zmin, minimal zoom level (default 4)

    --store, store tiles on filesystem

=cut

# options var
my $simulate = 0;
my $timeout = 30;
my ($zmin, $zmax) = (4, 6);
my $minwait = 2;
my $maxwait = 30;
my $bbox = "-2,40,2,42";
my $point = "";
my ($debug, $force, $quiet, $help, $verbose, $version, $purge, $store);
my $result = GetOptions ( "bbox=s" => \$bbox,
			  "point=s" => \$point,
			  "debug" => \$debug,
			  "force" => \$force,
			  "help" => \$help,
			  "layer=s" => \@layers,
			  "purge" => \$purge,
			  "minwait=i" => \$minwait,
			  "server=s" => \@servers,
			  "simulate" => \$simulate,
			  "timeout=i" => \$timeout,
			  "quiet" => \$quiet,
			  "verbose" => \$verbose,
			  "version" => \$version,
			  "store=s" => \$store,
			  "zmin=i" => \$zmin,
			  "zmax=i" => \$zmax);

# working vars
my $command = 'GET';
my $wait = $minwait;
my ($incache, $size, $calls, $tiles) = (0, 0, 0, 0);
my ($goodstatus, $globalstatus) = (0, 0);
my $agent = "fill-cache.pl ".VERSION;

# exit in case of bad or ambiguous option
exit 1 unless $result;

if ( $version ) {
    printf "fill-cache.pl %s\n", VERSION;
    exit 0;
}

usage() if ( $help );

die sprintf("minwait can't be negative %d", $minwait) if ( $minwait < 0 );
die sprintf("timeout can't be negative %d\n", $timeout) if ( $timeout < 0 );

if ( scalar(@layers) == 0) {
    print "Need at least one layer\n";
    exit 1;
}

print "--force option used, never stop\n" if ( $force );

if ( scalar(@servers) == 0) {
    print "Need at least one server\n";
    exit 1;
}

# Remove duplicates server, and initialize status
foreach (@servers) {
    my $key = "";
    my @infos = split /:/, $_;
    if ( scalar(@infos) > 1 ) {
	$key = $infos[0].":".$infos[1];
    } else {
	$key = $infos[0].":80";
    }
    $badstatus{$key} = $_;
}
(@servers) = (keys(%badstatus));
#

# Remove duplicates layer
my (%hlayer);
$hlayer{$_} = $_ foreach (@layers);
(@layers) = (keys(%hlayer));
#

$command = 'PURGE' if $purge;
$wait = 0 if $purge;

# check correct values on zoom
$zmax = 18 if $zmax > 18;
$zmin = 0 if $zmin < 0;

my $firstserver = pop(@servers);


if ($point) {
    getpoint($point, $zmin, $zmax);
} else {
    getbbox($bbox, $zmin, $zmax);
}

sub usage() {
    print "fill-cache --layer=foobar --server=host.domain.ext --bbox=X,X,X,X [--server=secondname] [--store]\n";
    exit 0;
}

my $cstep;

sub getbbox() {

    $bbox = shift;
    $zmin = shift;
    $zmax = shift;

    my ($lonmin, $latmin, $lonmax, $latmax) = split(",", $bbox);
    printf "From %d,%d to %d,%d (EPSG:4326)\n", $lonmin, $latmin, $lonmax, $latmax if $verbose;
    
    my $z = $zmin;
    my ($total, $nb) = (0, 0);
    
    while ($z <= $zmax) {
	my ($xmin, $ymax) = getTileNumber ($latmin, $lonmin, $z);
	my ($xmax, $ymin) = getTileNumber ($latmax, $lonmax, $z);
	$nb = ( ($ymax - $ymin + 1 ) * ( $xmax - $xmin + 1 ) );
	$total = $total + $nb;
	printf "Zoom level %2d : %d tiles\n", $z, $nb if !$quiet;
	$z++;
    }
    my $tcalls = $total * @layers * ( @servers + 1 );


    printf "Nb zoom levels %2d : %d tiles\n", ($zmax - $zmin + 1), $total if !$quiet;
    printf "%d servers, %d layers -> %d http requests\n", @servers + 1, scalar(@layers), $tcalls if !$quiet;

    my ($i, $s) = (0, 10);
    $cstep = ( int($tcalls / 10) > 1 ) ? int( $tcalls / 10) : 0;
    
    # stop here if we just want to simulate
    exit if $simulate;
    
    # we start at zmin
    $z = $zmin;

    # main loop   
    while ($z <= $zmax) {
	
	my ($xmin, $ymax) = getTileNumber ($latmin, $lonmin, $z);
	my ($xmax, $ymin) = getTileNumber ($latmax, $lonmax, $z);
	
	$ymin = 0 if $ymin < 0;
	$xmin = 0 if $xmin < 0;
	
	printf "z=%d %d,%d to %d,%d\n",$z,$xmin,$ymin,$xmax,$ymax if $verbose;

	fill_loop($ymin, $ymax, $xmin, $xmax, $z);
	
	if ( !$purge && !$quiet ) {
	    printf "Zoom level %d finished, %d tiles cached, %s/%s (added to cache/already in)\n", $z, $tiles, pretty_size($size), pretty_size($incache);
	}
	$z++;
    }    

    # End
    if (!$quiet) {
	printf "HTTP Code %s : %d/%d %.2f%%\n", $_, $status_counter{$_}, $calls, ($status_counter{$_}/ $calls * 100 ) foreach (keys(%status_counter));
    }

}


sub getpoint() {

    $point = shift;
    $zmin = shift;
    $zmax = shift;

    my $xsize = 3;
    my $ysize = 3;

    my ($ptlon, $ptlat) = split(",", $point);
    printf "Point %d,%d  (EPSG:4326)\n", $ptlon, $ptlat if $verbose;
    
    my $z = $zmin;
    my ($total, $nb) = (0, 0);
    
    while ($z <= $zmax) {
	$nb = (2 * $xsize + 1) * (2 * $ysize + 1);
	$total = $total + $nb;
	printf "Zoom level %2d : %d tiles\n", $z, $nb if !$quiet;
	$z++;
    }
    my $tcalls = $total * @layers * ( @servers + 1 );

    my ($i, $s) = (0, 10);
    $cstep = ( int($tcalls / 10) > 1 ) ? int( $tcalls / 10) : 0;
    
    # stop here if we just want to simulate
    exit if $simulate;
    
    # we start at zmin
    $z = $zmin;

    # main loop   
    while ($z <= $zmax) {
	
	my ($ptx, $pty) = getTileNumber ($ptlat, $ptlon, $z);
	
	my $xmin = $ptx - $xsize;
	my $xmax = $ptx + $xsize;
	my $ymin = $ptx - $ysize;
	my $ymax = $ptx + $ysize;
	
	$ymin = 0 if $ymin < 0;
	$xmin = 0 if $xmin < 0;
	
	printf "z=%d %d,%d to %d,%d\n",$z,$xmin,$ymin,$xmax,$ymax if $verbose;

	fill_loop($ymin, $ymax, $xmin, $xmax, $z);

	if ( !$purge && !$quiet ) {
	    printf "Zoom level %d finished, %d tiles cached, %s/%s (added to cache/already in)\n", $z, $tiles, pretty_size($size), pretty_size($incache);
	}
	$z++;
    }    

    # End
    if (!$quiet) {
	printf "HTTP Code %s : %d/%d %.2f%%\n", $_, $status_counter{$_}, $calls, ($status_counter{$_}/ $calls * 100 ) foreach (keys(%status_counter));
    }

}


sub fill_loop {
    my $ymin = shift;
    my $ymax = shift;
    my $xmin = shift;
    my $xmax = shift;
    my $z = shift;
   
    my $i = 0;
    my $y = $ymin;
    while ($y <= $ymax) {	
	my $x = $xmin;
	
	while ($x <= $xmax) {
	    foreach my $layer (@layers) {
		fetchfill($layer, $firstserver, $store, $z, $x, $y) if $badstatus{$firstserver} < 10;
		$i++;
	    }
	    sleep($wait);
	    foreach my $layer (@layers) {		    
		foreach (@servers) {
		    fetchfill($layer, $_, $z, $x, $y) if $badstatus{$_} < 10;
		    $i++;
		}
	    }
	    if ($globalstatus > 9 && !$force) {
		print "More than 10 consecutive errors on all servers, stop here\n";
		exit 1;
	    }
	    $x++;
	}
	$y++;
    }
}

sub progress {
    my ($i) = @_;
    my $s;
    if ( $i == $cstep ) {
	printf "%d%%\n", $s if !$quiet;
	$s = $s + 10 ;
	$cstep = $cstep + $cstep;
    }
}

sub fetchfill {
    my ($layer, $server, $store, $z, $x, $y) = @_;
    my $URL = "http://$server/%s/%d/%d/%d.png";
    my $url = sprintf $URL, $layer, $z,$x,$y;
    if ($layer eq "none") {
        $URL = "http://$server/%d/%d/%d.png";
        $url = sprintf $URL, $z,$x,$y;
    }   
    
    if ($debug == 0 ) {
	my $ua = LWP::UserAgent->new( agent => $agent,
				      timeout => $timeout );
	
	my $request = new HTTP::Request($command, $url); 
	$request->header('Host', split /:/, $server); 
	my $response = $ua->request($request);
	
	if (!$response->is_success) {
	    # HTTP call failed
	    $globalstatus++;
	    $badstatus{$server} = $badstatus{$server} + 1;
	} else {
	    if ( $response->header('Age') == 0 ) {
            $size = $size + $response->header('Content-Length');
	    } else {
            $incache = $incache + $response->header('Content-Length');
	    }
        if ($store) {
            my $directory = $store . sprintf "/%d", $z;
            unless(-e $directory or mkdir $directory) {
                die "Unable to create $directory";
            }
            $directory = $store . sprintf "/%d/%d", $z, $x;
            my $filename = "$directory/$y.png";
            unless(-e $directory or mkdir $directory) {
                die "Unable to create $directory";
            }
            open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";
            print $fh $response->content;
            close $fh;
        }


	    $tiles++;
	    $goodstatus++;
	    $globalstatus = ( $globalstatus > 0 ) ? $globalstatus - 1 : 0;
	    $badstatus{$server} = ( $badstatus{$server} > 0 ) ? $badstatus{$server} - 1 : 0;
	}

	$status_counter{$response->code}++;

	if ($response->code == "503") {
	    $wait++;
	    $goodstatus = 0;
	    if ( $wait > $maxwait ) {
		printf STDERR "wait is now too long, we stop here\n";
		exit 1;
	    } else {
		printf STDERR "got a 503 from %s, increase wait by 1 sec, wait now %s sec\n", $server, $wait;
	    }
	}

	if ( $badstatus{$server} > 9 && !$force ) {
	    printf STDERR "More than 10 consecutive errors on server %s, remove it from spool\n", $server;
	}
	
	if ( $goodstatus > 100 && $wait > $minwait && !$force) {
	    $wait = $wait - 1;
	    printf STDERR "100 call without errors, decrease wait by 1 sec, wait now %s sec\n", $wait;
	    $goodstatus = 0;
	}	
	$calls++;

	printf "%s %s %s\n", $command, $response->code, $url if $verbose;

    } 
}

sub getTileNumber {
    my ($lat, $lon, $zoom) = @_;
    my $xtile = int( ($lon+180)/360 *2**$zoom ) ;
    my $ytile = int( (1 - log(tan(deg2rad($lat)) + sec(deg2rad($lat)))/pi)/2 *2**$zoom ) ;
    return ($xtile, $ytile);
}

sub pretty_size {
    my ($size) = @_;

    if ( $size > 1024 * 1024 * 1024 ) {
	$size = sprintf "%.2f GBytes", $size / ( 1024 * 1024 * 1024 );
    } elsif ( $size > 1024 * 1024 ) {
	$size = sprintf "%.2f MBytes", $size / ( 1024 * 1024 ) ;
    } elsif ( $size > 1024 ) {
	$size = sprintf "%.2f KBytes", $size / 1024;
    } else {    
	$size = sprintf "%.2f Bytes", $size ;
    }

    return $size;
}

=head1 REQUIREMENTS

    fill-cache.pl use LWP and Getop::Long, if you use Debian install
    these two packages.

    - libgetopt-mixed-perl                                                                                                                                                                 
    and
 
    - libwww-perl   


=head1 AUTHOR

    Rodolphe Quiedeville <rodolphe@quiedeville.org>

=head1 LICENCE

    GPLv3 licence

=head1 SEE ALSO

    Last versions of fill-cache.pl are available at https://gitorious.org/osmtools

=cut
