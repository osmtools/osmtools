#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ovpwild execute as many query as necessary to cover the bbox get on
 command line options by square size of 1°x1° and store each request
 in a different file.

"""
import sys

def main():
    """
    main function
    """
    print '''
<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="keyboard">
<note>Created by Rodolphe Quiedeville</note>
'''

    nid = 1

    lat = -91
    while lat < 90:
        lat = lat + 1

        print '''
  <node id="{ida}" lat="{lat}" lon="-180"/>
  <node id="{idb}" lat="{lat}" lon="180"/>

  <way id="{idc}">
    <nd ref="{ida}"/>
    <nd ref="{idb}"/>
    <tag k="name" v="{lat}"/>
  </way>'''.format(lat=lat, ida=nid, idb=nid+1, idc=nid+2)
        nid = nid + 3

    lon = -181
    while lon < 180:
        lon = lon + 1

        print '''
  <node id="{ida}" lat="-89" lon="{lon}"/>
  <node id="{idb}" lat="89"  lon="{lon}" />

  <way id="{idc}">
    <nd ref="{ida}"/>
    <nd ref="{idb}"/>
    <tag k="name" v="{lon}"/>
  </way>'''.format(lon=lon, ida=nid, idb=nid+1, idc=nid+2)
        nid = nid + 3

    print "</osm>"

if __name__ == "__main__":
    sys.exit(main())
